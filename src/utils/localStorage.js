let storage = window.localStorage

export function getStorage (key) {
  return JSON.parse(storage.getItem(key))
}

export function setStorage (key, value) {
  storage.setItem(key, JSON.stringify(value))
}

export function clearAllStorage () {
  storage.clear()
}
