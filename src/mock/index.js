import Mock from 'mockjs'

// login
Mock.mock(RegExp('/api/login' + '.*'), 'post', (req, res) => {
  let data = JSON.parse(req.body)
  const success = {
    success: true,
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoid2FuZ2hhbyIsImFkbWluIjp0cnVlLCJpYXQiOjE1MjkwMzUzODYsImV4cCI6MTUyOTEyMTc4Nn0.mkNrt4TfcfmP22xd3C_GQn8qnUmlB39dKT9SpIBTBGI',
    user: { username: 'admin' }
  }
  const failed = {
    success: false
  }
  return data.username === 'admin' && data.password === '123456' ? success : failed
})

// layout
const routesAsync = [
  {
    code: 'platform',
    name: '平台配置',
    icon: 'el-icon-menu',
    children: [
      { code: 'dictionary', name: '数据字典' },
      { code: 'pageGenerate', name: '页面生成器' },
      { code: 'sqlMonitor', name: 'SQL监控' }
    ]
  },
  {
    code: 'system',
    name: '系统配置',
    icon: 'el-icon-setting',
    children: [
      { code: 'menuConfig', name: '菜单配置' },
      { code: 'roleConfig', name: '角色配置' },
      { code: 'roleUser', name: '角色对应用户' }
    ]
  }
]

Mock.mock('/api/getMenuList', 'get', () => {
  return routesAsync
})
