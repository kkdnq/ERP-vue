import request from './index'

export function apiGetMenuList (params) {
  return request({
    url: 'getMenuList',
    method: 'get'
  })
}
