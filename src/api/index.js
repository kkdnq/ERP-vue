import Axios from 'axios'

const service = Axios.create({
  baseURL: '/api/',
  timeout: 5000
})

service.interceptors.request.use(config => {
  config.headers['X-Token'] = 'askajsdcbksdjbvaksjbdksbdcksjbvs'
  config.headers['Content-Type'] = 'application/json'
  return config
})

export default service
