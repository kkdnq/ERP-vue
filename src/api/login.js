import request from './index'

export function apiLogin (data) {
  return request({
    url: 'login',
    method: 'post',
    data
  })
}
