const getters = {
  tags: state => state.tags,
  sidebar: state => state.sidebar
}

export default getters
