import Vue from 'vue'
import Vuex from 'vuex'

// import getters from './getters'
import app from './app'
import tags from './tags'
import sidebar from './sidebar'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    tags,
    sidebar
  }
})

export default store
