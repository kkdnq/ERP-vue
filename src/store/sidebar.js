const sidebar = {
  state: {
    menuList: []
  },
  mutations: {
    SET_LIST (state, list) {
      state.menuList = list
    }
  },
  actions: {
    setMenuList ({ commit }, list) {
      commit('SET_LIST', list)
    }
  },
  getters: {
    menuList (state) {
      return state.menuList
    }
  }
}

export default sidebar
