const app = {
  state: {
    user: null,
    sidebar: {
      opened: false
    }
  },
  mutations: {
    SET_USER (state, user) {
      state.user = user
    },
    changeSidebar (state) {
      state.sidebar.opened = !state.sidebar.opened
    }
  },
  actions: {
    setUser ({ commit }, user) {
      commit('SET_USER', user)
    }
  },
  getter: {
    user (state) {
      return state.user
    }
  }
}

export default app
