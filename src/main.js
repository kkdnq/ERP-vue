import Vue from 'vue'
import App from './App.vue'

// normalize
import 'normalize.css/normalize.css'

// element
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

// index.css
import '@/styles/index.scss'

// store
import store from './store'

// router
import router from './router'

// mock
import '@/mock/index.js'

Vue.config.productionTip = false

Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
