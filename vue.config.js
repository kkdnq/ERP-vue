const resolve = dir => {
  return require('path').join(__dirname, dir)
}

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  // 不生成 map 资源
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8546,
    host: 'localhost',
    https: false
    // 代理设置
    // proxy: {
    //   '/api': {
    //     target: 'http://localhost:8080/api', // 对应自己的接口
    //     changeOrigin: true,
    //     ws: true,
    //     pathRewrite: {
    //       '^/api': '/'
    //     }
    //   }
    // }
  },
  // 全局 css
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/styles/index.scss";
        `
      }
    }
  },
  // 自定义路径
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('@a', resolve('src/api'))
      .set('@v', resolve('src/views'))
      .set('@c', resolve('src/components'))
  }
}
